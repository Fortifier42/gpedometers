package net.theausfort.gpedometers;

import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.icu.text.SimpleDateFormat;

/**
 * Created by jtrho on 10/10/2017.
 */

public class DBManager
        extends SQLiteOpenHelper
{

    private static final String DATABASE_NAME    = "Statistics.db";
    private static final String TABLE_NAME       = "distances";
    private static final int    DATABASE_VERSION = 2;

    private static final String KEY_ID       = "date";
    private static final String KEY_DISTANCE = "distance";



    public DBManager(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }



    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "( " + KEY_ID + " INTEGER PRIMARY KEY NOT NULL, " +
                              KEY_DISTANCE + " REAL NOT NULL)";
        db.execSQL(CREATE_TABLE);
    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        if (oldVersion < newVersion)
        {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }



    public boolean dayExists(Date date)
    {
        SQLiteDatabase db     = this.getReadableDatabase();
        String         query  = "SELECT date FROM " + TABLE_NAME + " WHERE date='" + formatDate(date) + "'";
        Cursor         cursor = db.rawQuery(query, null);
        boolean        exists = cursor.getCount() > 0;
        cursor.close();
        return exists;
    }



    public void updateDay(Date date, double distance)
    {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, formatDate(date));
        values.put(KEY_DISTANCE, distance);

        if (dayExists(date))
        {
            values.put(KEY_DISTANCE, distance + getDistance(date));
            db.update(TABLE_NAME, values, KEY_ID + " = ?", new String[]{formatDate(date)});
        }
        else
        {
            db.insert(TABLE_NAME, null, values);
        }

    }



    public double getDistance(Date date)
    {
        if (!dayExists(date))
        {
            return 0;
        }
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{KEY_DISTANCE}, KEY_ID + "=?", new String[]{
                formatDate(date)}, null, null, null, null);
        if (cursor == null)
        {
            return 0;
        }
        cursor.moveToNext();
        double distance = cursor.getDouble(0);
        cursor.close();
        return distance;
    }



    public double getTotalDistance()
    {
        SQLiteDatabase db       = getReadableDatabase();
        String         query    = "SELECT distance from " + TABLE_NAME;
        Cursor         cursor   = db.rawQuery(query, null);
        double         distance = 0;
        while (cursor.moveToNext())
        {
            distance += cursor.getDouble(0);
        }
        cursor.close();
        return distance;
    }



    public String formatDate(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
        return format.format(date);
    }

}
