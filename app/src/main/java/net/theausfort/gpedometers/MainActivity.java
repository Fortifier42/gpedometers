package net.theausfort.gpedometers;

import java.util.Date;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.icu.text.DecimalFormat;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.SphericalUtil;
import com.squareup.picasso.Picasso;
import com.timgroup.jgravatar.Gravatar;
import com.timgroup.jgravatar.GravatarDefaultImage;
import com.timgroup.jgravatar.GravatarRating;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class MainActivity
        extends AppCompatActivity
        implements View.OnClickListener
{

    public static final  String TAG                                     = "MainActivity";
    private static final int    REQUEST_PERMISSIONS_REQUEST_CODE        = 34;
    private static final int    REQUEST_CHECK_SETTINGS                  = 0x1;
    private static final long   UPDATE_INTERVAL_IN_MILLISECONDS         = 10000;
    private static final long   FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    public static  double                      mTempDistance;
    public static  double                      sessionDistance;
    private static DBManager                   dbManager;
    private        FusedLocationProviderClient mFusedLocationClient;
    private        SettingsClient              mSettingsClient;
    private        LocationRequest             mLocationRequest;
    private        LocationSettingsRequest     mLocationSettingsRequest;
    private        LocationCallback            mLocationCallback;
    private        LatLng                      mCurrentLatLng;
    private        long                        lastSync;



    public static DBManager getDbManager()
    {
        return dbManager;
    }



    @Override
    public void onBackPressed()
    {
    }



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final DecimalFormat decimalFormat = new DecimalFormat("#.##");

        TextView  nameTextView     = (TextView) findViewById(R.id.userProfileName);
        ImageView profileImageView = (ImageView) findViewById(R.id.userProfileImage);

        nameTextView.setText(UserManager.getUserDisplayName());
        Gravatar gravatar = new Gravatar();
        gravatar.setRating(GravatarRating.GENERAL_AUDIENCES);
        gravatar.setDefaultImage(GravatarDefaultImage.IDENTICON);

        Picasso.with(this).load(gravatar.getUrl(UserManager.getUserEmail().toString())).into(profileImageView);

        final TextView distanceToday   = (TextView) findViewById(R.id.distanceToday);
        final TextView distanceTotal   = (TextView) findViewById(R.id.distanceTotal);
        final TextView distanceSession = (TextView) findViewById(R.id.distanceSession);

        final Handler loopHandler = new Handler();
        loopHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                distanceToday
                        .setText(getString(R.string.distance_today,
                                           decimalFormat.format(MainActivity.getDbManager().getDistance(new Date()))));
                distanceTotal
                        .setText(getString(R.string.distance_total,
                                           decimalFormat.format(MainActivity.getDbManager().getTotalDistance())));
                distanceSession
                        .setText(getString(R.string.distance_session,
                                           decimalFormat.format(MainActivity.sessionDistance)));

                loopHandler.postDelayed(this, 5000);
            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        createLocationRequest();
        createLocationCallback();
        buildLocationSettingsRequest();

        requestPermissions();

        dbManager = new DBManager(this);
    }



    @Override
    protected void onStart()
    {
        super.onStart();

        lastSync = System.currentTimeMillis();
        sessionDistance = 0;
        mTempDistance = 0;
    }



    private void requestPermissions()
    {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                                                                    Manifest.permission.ACCESS_FINE_LOCATION);

        if (shouldProvideRationale)
        {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                         android.R.string.ok, new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                                              new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                                              REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        }
        else
        {
            Log.i(TAG, "Requesting permission");
            ActivityCompat.requestPermissions(MainActivity.this,
                                              new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                              REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }



    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener)
    {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }



    private void startLocationUpdates()
    {
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                       .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>()
                       {
                           @Override
                           public void onSuccess(LocationSettingsResponse locationSettingsResponse)
                           {
                               Log.i(TAG, "All location settings are satisfied.");

                               //noinspection MissingPermission
                               mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                                                           mLocationCallback, Looper.myLooper());

                           }
                       })
                       .addOnFailureListener(this, new OnFailureListener()
                       {
                           @Override
                           public void onFailure(@NonNull Exception e)
                           {
                               int statusCode = ((ApiException) e).getStatusCode();
                               switch (statusCode)
                               {
                                   case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                       Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                                  "location settings ");
                                       try
                                       {
                                           // Show the dialog by calling startResolutionForResult(), and check the
                                           // result in onActivityResult().
                                           ResolvableApiException rae = (ResolvableApiException) e;
                                           rae.startResolutionForResult(MainActivity.this, REQUEST_CHECK_SETTINGS);
                                       }
                                       catch (IntentSender.SendIntentException sie)
                                       {
                                           Log.i(TAG, "PendingIntent unable to execute request.");
                                       }
                                       break;
                                   case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                       String errorMessage = "Location settings are inadequate, and cannot be " +
                                                             "fixed here. Fix in Settings.";
                                       Log.e(TAG, errorMessage);
                                       Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                               }

                           }
                       });
    }



    private void buildLocationSettingsRequest()
    {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }



    private void createLocationCallback()
    {
        mLocationCallback = new LocationCallback()
        {
            @Override
            public void onLocationResult(LocationResult locationResult)
            {
                super.onLocationResult(locationResult);

                Location newLocation = locationResult.getLastLocation();

                LatLng newLatLng = new LatLng(newLocation.getLatitude(), newLocation.getLongitude());

                if (mCurrentLatLng != null)
                {
                    mTempDistance += SphericalUtil.computeDistanceBetween(mCurrentLatLng, newLatLng);
                    syncDistance();
                }

                mCurrentLatLng = newLatLng;
            }
        };
    }



    private void createLocationRequest()
    {
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE)
        {
            if (grantResults.length <= 0)
            {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            }
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                startLocationUpdates();
            }
            else
            {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation,
                             R.string.settings, new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }



    private void syncDistance()
    {
        sessionDistance += mTempDistance;
        // Any faster than 6 meters per second and you're obviously not running.
        long   seconds      = MILLISECONDS.toSeconds(System.currentTimeMillis() - lastSync);
        double averageSpeed = mTempDistance / seconds;
        if (averageSpeed < 6)
        {
            dbManager.updateDay(new Date(), mTempDistance);
        }
        lastSync = System.currentTimeMillis();
        mTempDistance = 0;
    }



    @Override
    protected void onStop()
    {
        super.onStop();

        syncDistance();
    }



    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.manageProfileButton)
        {
            super.onBackPressed();
        }
    }
}
