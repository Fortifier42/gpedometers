package net.theausfort.gpedometers;

import android.net.Uri;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by jtrho on 29/09/2017.
 */

public class UserManager
{

    private static UserManager instance;

    private GoogleSignInAccount account;



    public UserManager()
    {
        account = null;
    }



    public static void setAccount(GoogleSignInAccount account)
    {
        getInstance().account = account;
    }



    public static GoogleSignInAccount getAccount()
    {
        return getInstance().account;
    }



    public static boolean signedIn()
    {
        return getInstance().account != null;
    }



    public static UserManager getInstance()
    {
        if (instance == null)
        {
            instance = new UserManager();
        }
        return instance;
    }



    public static CharSequence getUserDisplayName()
    {
        return getInstance().account.getDisplayName();
    }



    public static CharSequence getUserFirstName()
    {
        return getInstance().account.getGivenName();
    }



    public static CharSequence getUserLastName()
    {
        return getInstance().account.getFamilyName();
    }



    public static CharSequence getUserEmail()
    {
        return getInstance().account.getEmail();
    }



    public static Uri getUserProfileImage()
    {
        return getInstance().account.getPhotoUrl();
    }
}
